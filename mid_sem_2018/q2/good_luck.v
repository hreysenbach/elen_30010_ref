module good_luck;
    reg [3:0] x;
    reg [0:3] y;
    integer     i;
    initial begin
        x = 5; y = 3;
        $displayb("1) ", x);
        $displayb("2) ", y);
        $displayb("3) ", x+y);
        x <= y; y = 1;
        $displayb("4) ", x);
        #1
        $display("5) The time is ", $time);
        y = 2;
        $displayb("6) ", x);
        x = 8'b1111_1010;
        $displayb("7) ", x);
        x = 4'b1010;
        $displayb("8) ", !x);
        $displayb("9) ", ~x);

        for (i = 0; i < 16; i = i + 1) begin
            x = i;
            $display("10) x = %d |x = %b ", i, |x);
        end
    end
endmodule
