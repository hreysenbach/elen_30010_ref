module Wilma;
    reg     [7:0]   x,y,z,t;
    reg             clk;

    initial begin
        $monitor($time, x,y,z,t);
        x = 0;
        y = 1;
        z = 2;
        t = 3;
        clk = 0;
        repeat (4) #1 clk = !clk;
    end

    always @(posedge clk) begin
        x <= y;
        y <= x;
        z = t;
        t = z;
    end
endmodule
