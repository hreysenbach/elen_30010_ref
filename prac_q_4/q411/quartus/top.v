module top (
    output  [4:0]       user_led,
    input   [3:0]       user_pb,
    input   [5:0]       user_dipsw
);

wire    not_z;

assign user_led[0] = !not_z;
assign user_led[4:1] = 4'b1111;

tricky inst0 (.x(user_pb[2]), .y(user_pb[3]), .z(not_z));


endmodule
