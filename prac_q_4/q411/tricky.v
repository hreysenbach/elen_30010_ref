module tricky (input x,y, output reg z);
always @(x)
    z <= x^y;
endmodule
