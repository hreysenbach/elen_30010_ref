module clk(clk, seconds, minutes, hours);

input               clk;
output  [6:0]       seconds;
output  [6:0]       minutes;
output  [6:0]       hours;

reg     [6:0]       sec;
reg     [6:0]       min;
reg     [6:0]       hour;
integer             count;

assign out = counter;

initial begin
    counter <= 6'b000000;
end

always @(posedge clk) begin
    if (count >= 50000000) begin
        if (sec >= 59) begin
            if (min >= 59) begin
                hour <= (hour >= 23) ? 0 : hour + 1;
                min = 0;
            end else begin
                min <= min + 1;
            end
            sec <= 0;
        end else begin
            sec <= sec + 1;
        end
        count <= 0;
    end else begin
        count <= count + 1;
    end
end

endmodule
