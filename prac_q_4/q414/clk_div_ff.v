module clk_div_ff ( clk, div_clk);
input       clk;
output      div_clk;

reg         counter_ff0;
reg         counter_ff1;
reg         counter_ff2;
reg         counter_ff3;
reg         counter_ff4;

assign div_clk = counter_ff4;

initial begin
    counter_ff0 <= 1'b0;
    counter_ff1 <= 1'b0;
    counter_ff2 <= 1'b0;
    counter_ff3 <= 1'b0;
    counter_ff4 <= 1'b0;
end

always @(posedge clk) begin
    counter_ff0 <= ~counter_ff0;
end

always @(posedge counter_ff0) begin
    counter_ff1 <= ~counter_ff1;
end

always @(posedge counter_ff1) begin
    counter_ff2 <= ~counter_ff2;
end

always @(posedge counter_ff2) begin
    counter_ff3 <= ~counter_ff3;
end

always @(posedge counter_ff3) begin
    counter_ff4 <= ~counter_ff4;
end

endmodule
