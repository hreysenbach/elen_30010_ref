module clk_div_tb();

reg            clk;
integer         ticks;
wire            counter_out;
wire            ff_out;

clk_div_counter     inst0   (.clk(clk), .div_clk(counter_out));
clk_div_ff          inst1   (.clk(clk), .div_clk(ff_out));

initial begin
    clk <= 1'b0;
    repeat (10000) #1 clk = ~clk;
end

endmodule
