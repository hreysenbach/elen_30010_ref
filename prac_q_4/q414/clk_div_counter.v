module clk_div_counter ( clk, div_clk);
input       clk;
output      div_clk;

reg [4:0]   counter;

assign div_clk = counter[4];

initial begin
    counter <= 5'b01111;
end

always @(posedge clk) begin
    counter <= counter + 1;
end

endmodule
