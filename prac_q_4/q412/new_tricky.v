module new_tricky (input clk,x,y, output reg z);

reg     old_x;

initial begin
    z <= 1'b0;
end

always @(posedge clk) begin
    if (x != old_x) begin
        z <= x^y;
    end
    old_x <= x;
end

endmodule
