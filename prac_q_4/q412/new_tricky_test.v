module new_tricky_tb;

reg             x;
reg             y;
wire            z;
reg             clk;
integer         ticks;

new_tricky inst0    (.clk(clk), .x(x),.y(y),.z(z));

initial begin
    clk <= 1'b0;
    x <= 1'b0;
    y <= 1'b0;
    ticks <= 0;
    repeat (1000) #1 clk = ~clk;
end

always @(posedge clk) begin
    ticks <= ticks + 1;
    
    if (ticks == 2) begin
        x <= 1'b1;
        y <= 1'b0;
    end else if (ticks == 4) begin
        x <= 1'b1;
        y <= 1'b1;
    end else if (ticks == 5) begin
        x <= 1'b0;
        y <= 1'b1;
    end
end

endmodule
