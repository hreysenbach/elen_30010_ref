module top (
    output  [4:0]       user_led,
    input   [3:0]       user_pb,
    input   [5:0]       user_dipsw,
    input               clk50m_max10
);

wire    not_z;

assign user_led[0] = !not_z;
assign user_led[4:1] = 4'b1111;

new_tricky inst0 (.clk(clk50m_max10), .x(user_pb[2]), .y(user_pb[3]), .z(not_z));


endmodule
