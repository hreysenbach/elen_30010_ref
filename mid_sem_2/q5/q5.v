module q5 (input in, clk, output wire [1:0] out);
reg     [2:0]   state;

initial begin
    state = 3'b000;
end

assign out = (state == 3'b000) ? (2'b00) : 
            ((state == 3'b001) ? (2'b01) : 
            ((state == 3'b010) ? (2'b10) : 
            ((state == 3'b100) ? (2'b11) : 2'b00)));

always @(posedge clk) begin
    case (state)
        0 : begin
                if (in == 1'b0) begin
                    state <= 3'b001;
                end else begin
                    state <= 3'b100;
                end
            end
        1 : begin
                if (in == 1'b0) begin
                    state <= 3'b010;
                end else begin
                    state <= 3'b000;
                end
        end
        2 : begin
                if (in == 1'b0) begin
                    state <= 3'b001;
                end else begin
                    state <= 3'b100;
                end
        end
        4 : begin
                if (in == 1'b0) begin
                    state <= 3'b001;
                end else begin
                    state <= 3'b010;
                end
        end
        default : begin
                state <= 3'b010;
        end
    endcase
end
endmodule
