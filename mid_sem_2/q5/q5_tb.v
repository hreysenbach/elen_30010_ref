module q5_tb;
reg             in;
reg             clk;
wire    [1:0]   out;
integer         ticks;

q5 inst0 (.in(in), .clk(clk), .out(out));

initial begin
    clk <= 1'b0;
    in <= 1'b0;
    ticks <= 0;
    repeat (1000) #1 clk = ~clk;
end

always @(posedge clk) begin
    ticks <= ticks + 1;

    if (ticks == 1) begin
        in <= 1'b1;
    end else if (ticks == 2) begin
        in <= 1'b1;
    end else if (ticks == 3) begin
        in <= 1'b0;
    end else if (ticks == 4) begin
        in <= 1'b1;
    end else if (ticks == 5) begin
        in <= 1'b1;
    end else if (ticks == 6) begin
        in <= 1'b1;
    end else if (ticks == 7) begin
        in <= 1'b0;
    end
end

endmodule
