module state_machine (clk, in, out);
input clk;
input in;
output reg out;


integer state;


initial begin
    state <= 0;
end

always @(posedge clk) begin
    out <= 0'b0;
    case (state)
        0: 
            if (in == 1'b0) begin
                state <= 0;
            end else begin
                state <= 1;
            end
        1: 
            if (in == 1'b0) begin
                state <= 2; 
            end  else begin
                state <= 1;
            end
        2: 
            if (in == 1'b0) begin
                state <= 0;
            end else begin
                state <= 0;
                out <= 1'b1;
            end
        default: 
            state <= 0;
    endcase
end

endmodule
