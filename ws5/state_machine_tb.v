module state_machine_tb ();
reg clk;
reg in;
wire out;

integer ticks;

state_machine dut (.in(in), .out(out), .clk(clk));

initial begin
    in <= 0'b0;
    clk <= 0'b0;
    ticks <= 0;
    repeat (1000) #1 clk = ~clk;
end

always @(posedge clk) begin
    ticks <= ticks + 1;

    case (ticks)
        1: 
            in <= 1'b1;
        2: 
            in <= 1'b0;
        3:
            in <= 1'b1;
        4: 
            in <= 1'b0;
        5: 
            in <= 1'b1;
        6:
            in <= 1'b1;
        7:
            in <= 1'b0;
        8:
            in <= 1'b1;
        9:
            in <= 1'b1;
        10: 
            in <= 1'b0;
        11:
            in <= 1'b1;
        default:
            in <= 1'b0;
    endcase

end

endmodule
    
