module top (
    output  [4:0]       user_led,
    input   [3:0]       user_pb,
    input   [5:0]       user_dipsw,
    input               clk50m_max10
);

wire            not_out;
wire    [3:0]   debounce_pb;

assign user_led[0] = !not_out;
assign user_led[4:1] = 4'b1111;

debounce #(.debounce_target(1000000)) pb0 (.clk(clk50m_max10), .in(user_pb[0]), .out(debounce_pb[0]));
debounce #(.debounce_target(1000000)) pb1 (.clk(clk50m_max10), .in(user_pb[1]), .out(debounce_pb[1]));
debounce #(.debounce_target(1000000)) pb2 (.clk(clk50m_max10), .in(user_pb[2]), .out(debounce_pb[2]));
debounce #(.debounce_target(1000000)) pb3 (.clk(clk50m_max10), .in(user_pb[3]), .out(debounce_pb[3]));

state_machine inst0 (.clk(debounce_pb[2]), .in(debounce_pb[3]), .out(not_out));


endmodule
